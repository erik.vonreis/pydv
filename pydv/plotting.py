import re
import numpy as np
from matplotlib.ticker import Formatter, MaxNLocator
from matplotlib import cbook
from matplotlib.patches import Rectangle
import matplotlib.transforms as mtransforms
from matplotlib.figure import Figure
from matplotlib.axes import Axes

from gpstime import tconvert

from . import util
from . import const
from .bufferdict import NDSBufferDict


def _get_wd_state_key(fig):
    labels = []
    handles = []
    for state in const.WD_STATES:
        handles.append(Rectangle((0, 0), 1, 1, facecolor=const.WD_STATE_COLOR[state],
                       alpha=1.5*const.WD_STATE_SHADING_SETTINGS['alpha']))
        labels.append(str(state))

    legend = fig.legend(handles, labels, title='Watchdog state key')
    return legend


def _remove_wd_state_shading(ndsaxes):
    for shaded_region in ndsaxes.shaded_wd_state_regions:
        shaded_region.remove()


def _update_wd_state_shading(ndsaxes, channel_name, time_domain, data, end_time):
    _remove_wd_state_shading(ndsaxes)

    #shade_settings = const.WD_STATE_SHADING_SETTINGS
    wd_state_time_intervals = util.get_wd_state_time_intervals(time_domain, data, end_time)
    ndsaxes.shaded_wd_state_regions = _add_wd_state_shading(ndsaxes, wd_state_time_intervals,
                                                            **const.WD_STATE_SHADING_SETTINGS)


def _has_nds_parent_figure(a):
    return isinstance(a.figure, NDSFigure)


def _add_wd_state_shading(pyndsaxes, wd_state_intervals, **shade_settings):
    artists = []
    for wdsi in wd_state_intervals:
        color = const.WD_STATE_COLOR[wdsi.state]
        artists.append(pyndsaxes.axvspan(wdsi.start_time, wdsi.end_time, color=color, **shade_settings))
    return artists


def _update_line(pyndsaxes, channel_name, time_domain, data):
    line = pyndsaxes.line_map[channel_name]
    line.set_xdata(time_domain)
    line.set_ydata(data)


def _update_abs_threshold_line(pyndsaxes, channel_name, time_domain, data):
    # positive threshold line
    line = pyndsaxes.line_map[channel_name]
    line.set_xdata(time_domain)
    line.set_ydata(data)
    line.set_color(const.THRESHOLD_LINE_COLOR)

    # negative threshold line
    line = pyndsaxes.line_map[channel_name+'_neg']
    line.set_xdata(time_domain)
    line.set_ydata(-data)
    line.set_color(const.THRESHOLD_LINE_COLOR)


def _add_more_data(pyndsaxes):
    try:
        xmin, xmax = pyndsaxes.get_xlim()
        xmin += pyndsaxes.center_time
        xmax += pyndsaxes.center_time
        if pyndsaxes.pynds_bufferdict.add_data(xmin, xmax):
            for pyndsbuffer in pyndsaxes.pynds_bufferdict.buffers:
                time_domain = pyndsbuffer.time_domain - pyndsaxes.center_time
                end_time = pyndsbuffer.end_time-pyndsaxes.center_time
                if pyndsbuffer.is_wd_state:
                    _update_wd_state_shading(pyndsaxes, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data,
                                             end_time)
                elif pyndsbuffer.is_abs_threshold:
                    _update_abs_threshold_line(pyndsaxes, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data)
                else:
                    _update_line(pyndsaxes, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data)
            return True
    except (NDSBufferDict.ChannelRequestError, NDSBufferDict.ServerDownError, NDSBufferDict.BadBufferError) as e:
        pyndsaxes.logger.warning('Received NDSBufferDict error:\n'+str(e))
    return False


class NDSAxes(Axes):
    class Exception(Exception):
        pass

    class ChannelError(Exception):
        def __init__(self, channel_names):
            self.channel_names = channel_names

    logger = util.make_default_logger('NDSAxes')

    def __init__(self, fig, rect, xmin=None, xmax=None, center_time=None, channel_names=None, initial_plot_kwargs=None,
                 figure_updated_flag_sem_pair=None, no_threshold=False, no_wd_state=False, **kwargs):

        self.figure_updated_flag_sem_pair = figure_updated_flag_sem_pair

        if initial_plot_kwargs is None:
            initial_plot_kwargs = {}
        self.center_time = center_time
        
        # find out which channels should be shading channels
        def add_channel_to_mask(pattern):
            mask = []
            for name in channel_names:
                if re.match(pattern, name) is not None:
                    mask.append(True)
                    continue
                mask.append(False)
            if len(list(filter(lambda a: a, mask))) > 1:
                NDSAxes.logger.error('NDSAxes does not permit plotting more than one watchdog state or threshold channel in a special way.')
                raise NDSAxes.ChannelError(channel_names)
            return mask

        wd_state_mask = None
        if not no_wd_state:
            wd_state_mask = add_channel_to_mask('^.*_WD_?MON_STATE_.*$')
        abs_threshold_mask = None
        if not no_threshold:
            abs_threshold_mask = add_channel_to_mask('^.*_WD_.*_THRESH_MAX.*$')

        s = 'Initializing NDSAxes for the following channels:'
        for channel_name in channel_names:
            s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
        NDSAxes.logger.info(s)

        Axes.__init__(self, fig, rect, **kwargs)
        self.__last_xlim = self.get_xlim()

        self.line_map = {}
        self.shaded_wd_state_regions = []
        try:
            self.pynds_bufferdict = NDSBufferDict(channel_names, xmin, xmax, wd_state_mask=wd_state_mask,
                                                  abs_threshold_mask=abs_threshold_mask)
        except NDSBufferDict.NoNDSServerGiven as e:
            s = 'Unable to find NDS server or none specified: {}'.format(e)
            NDSAxes.logger.error(s)
            raise NDSAxes.Exception(e)

        except NDSBufferDict.ChannelRequestError as e:
            s = 'Unable to create NDSBufferDict for the following channels:'
            for channel_name in channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSAxes.logger.error(s)
            raise NDSAxes.ChannelError(channel_names)

        except NDSBufferDict.ServerDownError as e:
            s = 'Unable to connect to NDS server to retrieve channel data for:'
            for channel_name in channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSAxes.logger.error(s)
            raise NDSAxes.ChannelError(channel_names)

        except NDSBufferDict.BadBufferError as e:
            s = 'NDSBufferDict received a bad buffer for one of the following channels:'
            for channel_name in channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSAxes.logger.error(s)
            raise NDSAxes.ChannelError(channel_names)

        self.signal_channel_names = []

        self.__initial_formatting_before_adding_channels(xmin, xmax)

        self.add_channels(channel_names, **initial_plot_kwargs)

        self.__annotations = []
        self.__initial_formatting_after_adding_channels()

        if not _has_nds_parent_figure(self):
            self.figure.canvas.mpl_connect('button_release_event', self.get_more_data)

    def __initial_formatting_before_adding_channels(self, xmin, xmax):
        self.set_xlim(xmin-self.center_time, xmax-self.center_time)

    # Wake up get more data function when the x limits have changed and
    # the user releases the mouse button.
    #noinspection PyUnusedLocal
    def get_more_data(self, event):
        xlim = self.get_xlim()
        if self.__last_xlim != xlim:
            NDSAxes.logger.debug("x limits have changed. Adding more data...")
            self.__last_xlim = xlim
            more_data_added = _add_more_data(self)
            if _has_nds_parent_figure(self):
                return more_data_added
            self.figure.canvas.draw()
            return
        NDSAxes.logger.debug("x limits have not changed.")
        return False

    def __initial_formatting_after_adding_channels(self):

        # setup viewing 
        self.autoscale_view(scalex=False)

        # show grid lines on the plot
        self.grid(True)

        # define "x" and "y" labels
        self.set_xlabel('seconds after %f (s)' % self.center_time,
                        horizontalalignment='center')

        # setup x tick formatting
        class OffsetFormatter(Formatter):
            def __init__(self, center_time):
                self.center_time = center_time

            def __call__(self, x, pos=None):
                return ('+' if x > 0 else '') + str(x).replace('-', const.MINUS_SIGN)

        self.xaxis.set_major_formatter(OffsetFormatter(self.center_time))
        self.xaxis.set_major_locator(MaxNLocator(nbins=const.NUM_X_TICKS_BEFORE_PRUNING, prune='lower'))
        self.yaxis.set_major_locator(MaxNLocator(nbins=const.NUM_Y_TICKS_BEFORE_PRUNING, prune='both'))

        if not isinstance(self, NDSSubplot):
            self.set_title('%s (GPS %f) ' % (tconvert(str(self.center_time)),
                                             self.center_time,),
                           fontsize=const.NDSFIGURE_TITLE_FONTSIZE,
                           linespacing=const.NDSFIGURE_TITLE_LINESPACING,
                           bbox=const.AXES_TITLE_BBOX_ARGS)
            self.title.set_position(const.AXES_TITLE_POSITION)

            # make a draggable legend
            # TODO improve location of legend. placing legend above the axes seems like a good
            # idea. need to develop coordinates that shrink the axes so that the legend and axes
            # together always occupy a constant height.

            # remove the wd_state channel name from legend on each axes and
            # remove duplicate abs_threshold channel name from legend on each axes
            handles, labels = self.get_legend_handles_labels()
            indices_to_remove = []
            for i, channel_name in enumerate(labels):
                pydvbuffer = self.pynds_bufferdict[channel_name]
                if pydvbuffer.is_wd_state:
                    indices_to_remove.append(i)
                elif pydvbuffer.is_abs_threshold:
                    indices_to_remove.append(i)

            num_removed = 0
            for i in sorted(indices_to_remove):
                handles.pop(i-num_removed)
                labels.pop(i-num_removed)
                num_removed += 1
            legend = self.legend(handles, labels, prop=const.NDSAXES_LEGEND_FONTPROPERTIES_KWARGS)
            legend.draggable()

            there_is_wd_shading = False
            for pyndsbuffer in self.pynds_bufferdict.buffers:
                if pyndsbuffer.is_wd_state:
                    there_is_wd_shading = True

            if there_is_wd_shading:
                l = _get_wd_state_key(self)

        else:
            if True: #self.get_geometry()[0] * self.get_geometry()[1] > 1:
                self.set_title(self.signal_channel_names[0],
                               fontdict=const.NDSAXES_TITLE_FONTDICT,
                               bbox=const.AXES_TITLE_BBOX_ARGS)
                self.title.set_position(const.AXES_TITLE_POSITION)

        self.set_ylabel('magnitude (counts)', rotation='vertical')

        self.figure.canvas.mpl_connect('pick_event', self.annotate_pick)

    def line_picker(self, line, mouseevent):
        """
        find the points within a certain distance from the mouseclick in
        data coords and attach some extra attributes, pickx and picky
        which are the data points that were picked
        """
        if mouseevent.xdata is None:
            return False, dict()

        mouseevent_display_coords = self.transData.transform((mouseevent.xdata, mouseevent.ydata))
        mouseevent.xdisplay = mouseevent_display_coords[0]
        mouseevent.ydisplay = mouseevent_display_coords[1]

        line_display_coords = self.transData.transform(line.get_xydata())

        maxd = 5

        ydata = line.get_ydata()
        xdisplay = line_display_coords[:, 0]
        ydisplay = line_display_coords[np.logical_and((mouseevent.xdisplay-1) <= xdisplay,
                                                      xdisplay <= (mouseevent.xdisplay+1)), 1]

        d = np.abs(ydisplay-mouseevent.ydisplay)

        ind = np.nonzero(np.less_equal(d, maxd))
        if len(ind[0]):
            pickx = mouseevent.xdata
            picky = ydata[ind[0][0]]
            props = dict(pickx=pickx, picky=picky, channel_name=line.get_label())
            return True, props
        else:
            return False, dict()

    def remove_annotations(self):
        while self.__annotations:
            annotation = self.__annotations.pop()
            annotation.set_visible(False)
            annotation.remove()
            del annotation

    def annotate_pick(self, event):
        self.remove_annotations()

        if hasattr(event, 'channel_name'):
            annotation = self.annotate(event.channel_name, xy=(event.pickx, event.picky),
                                       xytext=(event.pickx, event.picky),
                                       bbox=const.ANNOTATION_BBOX_ARGS,
                                       zorder=5)
            self.__annotations.append(annotation)
            self.figure.canvas.draw()

    # TODO allow plotting new watchdog state shaded regions
    def add_channels(self, channel_names, **kwargs):
        self.pynds_bufferdict.add_channels(channel_names, **kwargs)
        signal_count = 0
        for channel_name in channel_names:
            pyndsbuffer = self.pynds_bufferdict[channel_name]
            time_domain = pyndsbuffer.time_domain-self.center_time
            end_time = pyndsbuffer.end_time-self.center_time
            if pyndsbuffer.is_wd_state:
                _update_wd_state_shading(self, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data, end_time)
            elif pyndsbuffer.is_abs_threshold:
                data = pyndsbuffer.data
                self.line_map[pyndsbuffer.channel_name+'_neg'] = self.plot(time_domain, -data,
                                                                           label=pyndsbuffer.channel_name,
                                                                           picker=self.line_picker, **kwargs)[0]
                data = pyndsbuffer.data
                self.line_map[pyndsbuffer.channel_name] = self.plot(time_domain, data, label=pyndsbuffer.channel_name,
                                                                    picker=self.line_picker, **kwargs)[0]
                _update_abs_threshold_line(self, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data)
            else:
                data = pyndsbuffer.data
                self.line_map[pyndsbuffer.channel_name] = self.plot(time_domain, data, label=pyndsbuffer.channel_name,
                                                                    picker=self.line_picker, **kwargs)[0]
                self.signal_channel_names.append(pyndsbuffer.channel_name)
                if isinstance(self, NDSSubplot):
                    signal_count += 1

        if signal_count > 1:
            l = self.legend()
            l.draggable()

    def remove_channel(self, channel_name):
        NDSAxes.logger.info("Client requested to remove "+channel_name+" from an instance of NDSAxes.")
        try:
            self.pynds_bufferdict.pop(channel_name)
        except KeyError:
            NDSAxes.logger.info(channel_name+" is not plotted on this instance of NDSAxes.")
            return
        if channel_name in self.line_map:
            line = self.line_map.pop(channel_name)
            line.remove()
            return
        _remove_wd_state_shading(self)


from matplotlib import axes
NDSSubplot = axes.subplot_class_factory(NDSAxes)


class SidebarTransform(mtransforms.Transform):
    def __init__(self, fig, offset=None):
        mtransforms.Transform.__init__(self)  # note that the shorthand_name kwarg does not exist in 1.1.1rc
        self.input_dims = 2
        self.output_dims = 2
        self.is_separable = False
        self.has_inverse = False
        self.fig = fig
        self.offset = offset

    def transform(self, values_raw):
        """
        Transform can either take a numpy array of shape (input_dims) or of (N, input_dims)

        So we have to handle both cases
        :param values: Either an array of length input_dims or of shape [N, input_dims]
        :return: Either an array of length output_dims or of shape [N, output_dims]
        """
        values = np.array(values_raw)
        if len(values.shape) == 1:
            return self._transform(values)
        else:
            ret_values = []
            for value in values:
                ret_values.append(self._transform(value))
            return np.array(ret_values)

    def _transform(self, value):
        upper_right_subplot = self.fig._get_upper_right_subplot()
        left_x_boundary = upper_right_subplot.transAxes.transform([[1,1]])[0][0]
        right_x_boundary = self.fig.transFigure.transform([[1,1]])[0][0]
        x = left_x_boundary+value[0]*(right_x_boundary-left_x_boundary)
        y = self.fig.transFigure.transform([[0,value[1]]])[0][1]
        if self.offset is not None:
            x += self.offset
        return [x,y]


class NDSFigure(Figure):
    logger = util.make_default_logger('NDSFigure')

    class Exception(Exception):
        pass

    def __repr__(self):
        return '%s(channel_names=%r, xmin=%r, xmax=%r, center_time=%r, shareax=%r, ' % (self.__class__.__name__, self.channel_names, self.xmin, self.xmax, self.center_time, self.shareax)\
                +'sharex=%r, sharey=%r, no_threshold=%r, no_wd_state=%r, ' % (self.sharex, self.sharey, self.no_threshold, self.no_wd_state)\
                +', '.join(['%s=%r' % item for item in self.__orig_kwargs.items()])+')'

    def __str__(self):
        newline = '\n          '
        r = '%s(channel_names= ... , xmin=%r, xmax=%r, center_time=%r, shareax=%r, ' % (self.__class__.__name__, self.xmin, self.xmax, self.center_time, self.shareax)\
                +'sharex=%r, sharey=%r, no_threshold=%r, no_wd_state=%r, ' % (self.sharex, self.sharey, self.no_threshold, self.no_wd_state)\
                +', '.join(['%s=%r' % item for item in self.__orig_kwargs.items()])+')'
        r = r.split(', ')
        s = self.__class__.__name__+'('
        r[0] = r[0][len(self.__class__.__name__)+1:]
        bracket_paren_stack = [1]  # mark the open parenthesis of NDSFigure()
        for part in r:
            for c in part:
                s += c
                if c in ['[','(','{']:
                    bracket_paren_stack.append(0)
                elif c in [']',')','}']:
                    if bracket_paren_stack.pop() == 1:  # if closed last parenthesis, return
                        return s
            s += ','
            if bracket_paren_stack.pop() == 1:  # check for straggling open parenthesis, curly brace, or bracket
                s += newline
                bracket_paren_stack.append(1)
            else:
                s += ' '
                bracket_paren_stack.append(0)
        return s  # should never be called if __repr__ and this function are defined correctly

    def __init__(self, channel_names=None, center_time=None, xmin=None, xmax=None,
                 title=None,
                 shareax=False, sharex=False, sharey=False,
                 no_threshold=False, no_wd_state=False,
                 **kwargs):

        Figure.__init__(self, **kwargs)

        s = 'Initializing NDSFigure with following channels:'
        for channel_name in set(cbook.flatten(channel_names)):
            s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
        NDSFigure.logger.info(s)

        self.channel_names = channel_names
        self.xmin = xmin
        self.xmax = xmax
        self.center_time = center_time
        if title:
            self.title_prefix = '%s\n' % title
        else:
            self.title_prefix = ''
        self.shareax = shareax
        self.sharex = sharex or shareax
        self.sharey = sharey or shareax
        self.subplot_updated = False
        self.no_threshold = no_threshold
        self.no_wd_state = no_wd_state
        self.__orig_kwargs = kwargs

        title = self.suptitle('%s%s (GPS %f)' % (self.title_prefix,
                                                 tconvert(str(self.center_time)),
                                                 self.center_time,),
                              fontsize=const.NDSFIGURE_TITLE_FONTSIZE,
                              linespacing=const.NDSFIGURE_TITLE_LINESPACING,
                              bbox=const.FIGURE_TITLE_BBOX_ARGS)
        title.set_position(const.FIGURE_TITLE_POSITION)

        # define "x" and "y" labels
        self.text(0.5, 0.03, 'seconds after %f (s)' % self.center_time,
                  horizontalalignment='center')
        self.text(0.02, 0.5, 'magnitude (counts)', verticalalignment='center',
                  horizontalalignment='left', rotation='vertical')

        self.__wd_state_key = None
        self.__first_draw = True

    def draw(self, renderer):
        Figure.draw(self, renderer)
        if self.__first_draw:
            self.__first_draw == False
            if self.__wd_state_key is not None:
                w = self.__wd_state_key.legendPatch.get_width()
                self.__wd_state_key.set_bbox_to_anchor((0.5, const.WD_STATE_LEGEND_Y_POS),
                                          transform=SidebarTransform(self, offset=w/2.+8))
                Figure.draw(self,renderer)

    def draw_subplots(self, subplot_dimensions=None):
        if subplot_dimensions is None:
            subplot_dimensions = util.get_best_subplot_dimensions(len(self.channel_names))
        assert(isinstance(subplot_dimensions, tuple))
        assert(len(subplot_dimensions)==2)
        subplot_rows, subplot_cols = subplot_dimensions

        there_is_wd_shading = False
        subplot_zorder = const.SUPLOT_ZORDER_START+len(self.channel_names)
        for fake_subplot_number, subplot_channel_names in enumerate(self.channel_names):
            subplot_number = fake_subplot_number + 1
            kwargs = dict(xmin=self.xmin,
                          xmax=self.xmax,
                          center_time=self.center_time,
                          channel_names=subplot_channel_names)

            if subplot_number > 1:
                if self.sharex:
                    kwargs['sharex'] = ax_to_share
                if self.sharey:
                    kwargs['sharey'] = ax_to_share

            try:
                subplot = NDSSubplot(self, subplot_rows, subplot_cols, subplot_number,
                                     no_threshold=self.no_threshold, no_wd_state=self.no_wd_state, **kwargs)
            except NDSSubplot.ChannelError as e:
                NDSFigure.logger.critical("NDSSubplot with the channels %r has failed." % e.channel_names)
                raise NDSFigure.Exception("Could not retrieve channels: {}".format(e.channel_names))


            if len(subplot.signal_channel_names) == 1:
                subplot.line_map[subplot.signal_channel_names[0]]\
                    .set_color(const.PLOT_COLORS[(subplot_number-1) % subplot_cols % len(const.PLOT_COLORS)])

            subplot.zorder = subplot_zorder
            subplot_zorder -= 1

            for pyndsbuffer in subplot.pynds_bufferdict.buffers:
                if pyndsbuffer.is_wd_state:
                    there_is_wd_shading = True

            if (self.sharex or self.sharey) and subplot_number == 1:
                ax_to_share = subplot

            s = 'Adding subplot with the following channels:'
            for channel_name in subplot_channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSFigure.logger.info(s)
            self.add_subplot(subplot)

            subplot.set_xlabel('')
            subplot.set_ylabel('')

        if there_is_wd_shading:
            # watchdog state shading legend
            self.__wd_state_key = _get_wd_state_key(self)

        if subplot_rows in const.FIG_SUBPLOTS_ADJUST:
            self.subplots_adjust(**const.FIG_SUBPLOTS_ADJUST[subplot_rows])

        self.canvas.mpl_connect('button_release_event', self.get_more_data)
        self.canvas.mpl_connect('button_release_event', self.remove_annotations)

        self.set_size_inches(const.SUBPLOT_INCHES_INITIAL_WIDTH+subplot_cols*const.SUBPLOT_INCHES_PER_COL,
                             const.SUBPLOT_INCHES_INITIAL_HEIGHT+subplot_rows*const.SUBPLOT_INCHES_PER_ROW,
                             forward=True)

    def _get_upper_right_subplot(self):
        if hasattr(self.axes[0], "get_geometry"):
            nrows, ncols, __ = self.axes[0].get_geometry()
        else:
            nrows, ncols, __, __ = self.axes[0].get_subplotspec().get_geometry()
        last_col = ncols
        for a in self.axes:
            if hasattr(self.axes[0], "get_geometry"):
                geom = a.get_geometry()
            else:
                geom_raw = a.get_subplotspec().get_geometry()
                geom = (geom_raw[0], geom_raw[1], geom_raw[2]+1)  # for compatibility

            if geom[const.SUBPLOT_NUMBER_INDEX] == last_col:
                return a

    def remove_annotations(self, mouseevent):
        if mouseevent.inaxes is None:
            for a in self.axes:
                if 'remove_annotations' in dir(a):
                    a.remove_annotations()
            self.canvas.draw()

    #noinspection PyUnusedLocal
    def get_more_data(self, event):
        redraw = False
        for a in self.axes:
            if hasattr(a, 'get_more_data'):
                if a.get_more_data(None):
                    redraw = True
        if redraw:
            self.canvas.draw()
